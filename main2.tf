terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_budgets_budget" "monthly-budget-maintf2" {
  name              = "monthly-budget"
  budget_type       = "COST"
  limit_amount      = "199"
  limit_unit        = "USD"
  time_period_start = "2020-12-15_00:01"
  time_unit         = "MONTHLY"


}





